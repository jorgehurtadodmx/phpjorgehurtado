<?php


// Ejercicio 3. Uso de fichero y filtro de palabras en el
// trabajar con ficheros.
// [https://gist.githubusercontent.com/jsdario/6d6c69398cb0c73111e49f1218960f79/raw/8d4fc4548d437e2a7203a5aeeace5477f598827d/el_quijote.txt](https://gist.githubusercontent.com/jsdario/6d6c69398cb0c73111e49f1218960f79/raw/8d4fc4548d437e2a7203a5aeeace5477f598827d/el_quijote.txt)

//escribir codigo que coja la ruta del fichero y diga cuantas veces aparece la palabra molina
// count
function wordFinder($palabra,$nombreFichero) {
    $ficheroAnalizado = file_get_contents($nombreFichero);

    echo substr_count($ficheroAnalizado,$palabra);
    echo "<br>";
    echo substr_count(strtoupper($ficheroAnalizado), strtoupper($palabra)); // para obtener todos los valores minusculas y mayusculas, convertimos todo el texto en mayusculas



}
// ejemplo de path : 'C:\xampp\htdocs\estudios1\el_quijote.txt' (depende del tuyo)

wordFinder('mis','C:\xampp\htdocs\estudios1\el_quijote.txt');


?>
<?php

// funcion que:
// EJERCICIO 5: crear programa que haga:
// crear directorio con nombre de fecha actual.
// copiar con mismo nombre.modificado un fichero de directorio actual


function ficheros() {
    $currentPath = getcwd(); //directorio actual
    $path = date("Y-m-d His"); // fecha actual. Formato modificable.
    mkdir("$currentPath/$path", 0777, true); //creación de directorio en directorio actual con permisos.
    //creacion de directorio de nombre (fecha actual) creado

    $originalFile = "$currentPath/$path" .".txt"; //variable de nombre: fichero con terminacion txt para su creación a posteriori.
    fopen($originalFile,'w+'); //creación de ficheros con permisos de escritura y lectura
    $copyFile = "$originalFile" . ".modificado"; //copia de la anterior con .txt.modficado.
    copy($originalFile,$copyFile); //creación de copia de fichero
    
}

ficheros();
// sin necesidad de pasar parametros cuando la intención es crear un directorio, fichero y copia del mismo con nombre de fecha actual.





















?>